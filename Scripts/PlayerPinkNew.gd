extends KinematicBody2D

export (int) var speed = 150

func _physics_process(delta):
	var motion = Vector2()
	motion.x = Input.get_action_strength("right") - Input.get_action_strength("left")
	motion.y = Input.get_action_strength("Down") - Input.get_action_strength("jump")
	
	move_and_slide(motion.normalized() * speed, Vector2())
