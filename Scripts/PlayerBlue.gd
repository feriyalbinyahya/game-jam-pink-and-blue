extends KinematicBody2D

export (int) var speed = 180
export (int) var GRAVITY = 1200
export (int) var push_speed = 125
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("AnimatedSprite")

func get_input():
	velocity.x = 0
	var flag_direction = 'idle'
	if is_on_floor() and Input.is_action_just_pressed('w'):
		velocity.y = jump_speed
		$sfx_jump.play()
	if Input.is_action_pressed('d'):
		velocity.x += speed
	if Input.is_action_pressed('a'):
		velocity.x -= speed
		flag_direction = 'left'
	return flag_direction

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	var flag = get_input()
	velocity = move_and_slide(velocity, UP)
	
	if get_slide_count()>0:
		check_box_collision(flag)

func _process(delta):
	if velocity.y != 0:
		animator.play("jump")
	elif velocity.x != 0:
		animator.play("walk")
		if velocity.x > 0:
			animator.flip_h = false
		else:
			animator.flip_h = true
	else:
		animator.play("idle")
		
func check_box_collision(flag):
	var box : = get_slide_collision(0).collider as BrownBox
	if box:
		box.push(push_speed, flag)
