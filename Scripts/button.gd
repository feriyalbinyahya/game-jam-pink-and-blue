extends LinkButton

export(String) var scene_to_load

func _on_mainmenu_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
func _on_playagain_pressed():
	global.bluescore = 0
	global.pinkscore = 0
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
