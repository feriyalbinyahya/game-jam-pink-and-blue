extends KinematicBody2D

export (int) var speed = 100

func _physics_process(delta):
	var motion = Vector2()
	motion.x = Input.get_action_strength("d") - Input.get_action_strength("a")
	motion.y = Input.get_action_strength("s") - Input.get_action_strength("w")
	
	move_and_slide(motion.normalized() * speed, Vector2())
