extends Area2D

var entered5 = false

func _on_Level5_body_entered(body):
	if body.get_name() == "PlayerBlueNew" and entered5 == false:
		entered5 = true
		global.bluescore += 1
		var blue = global.bluescore
		var pink = global.pinkscore
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		if blue > pink:
			get_tree().change_scene(str("res://Scenes/BlueCrown.tscn"))
		else:
			get_tree().change_scene(str("res://Scenes/PinkCrown.tscn"))
	elif body.get_name() == "PlayerPinkNew" and entered5 ==  false:
		entered5 = true
		global.pinkscore += 1
		var blue = global.bluescore
		var pink = global.pinkscore
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		if blue > pink:
			get_tree().change_scene(str("res://Scenes/BlueCrown.tscn"))
		else:
			get_tree().change_scene(str("res://Scenes/PinkCrown.tscn"))
