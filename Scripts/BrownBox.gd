extends KinematicBody2D
class_name BrownBox

const UP = Vector2(0,-1)

var velocity = Vector2()

func push(push_speed, flag) -> void:
	velocity.y += 1200
	if flag == 'left':
		velocity.x -= push_speed
	velocity = move_and_slide(velocity, UP)
