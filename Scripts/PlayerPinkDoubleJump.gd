extends KinematicBody2D

export (int) var speed = 180
export (int) var GRAVITY = 1200
export (int) var push_speed = 125
export (int) var jump_speed = -400
var double_jump = false
var on_ground = false

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var particle = self.get_node("Particles2D")
onready var animator = self.get_node("AnimatedSprite")

func get_input():
	velocity.x = 0
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false
	if Input.is_action_just_pressed('jump'):
		if on_ground == true:
			velocity.y = jump_speed
			double_jump = false
			on_ground = false
			$sfx_jump.play()
		elif on_ground == false and double_jump == false:
			velocity.y = jump_speed
			$sfx_jump.play()
			double_jump = true
			on_ground = false
	if Input.is_action_pressed("right"):
		velocity.x += speed
	if Input.is_action_pressed("left"):
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		animator.play("jump")
	elif velocity.x != 0:
		animator.play("walk")
		if velocity.x > 0:
			animator.flip_h = false
		else:
			animator.flip_h = true
	else:
		animator.play("idle")
		
	if is_on_floor() and (Input.is_action_pressed("left") or Input.is_action_pressed("right")):
		particle.set_emitting(true)
	else:
		particle.set_emitting(false)
