extends Area2D

var entered1 = false
var entered2 = false
var entered3 = false
var entered4 = false

func _on_Level1_body_entered(body):
	if body.get_name() == "Blue" and entered1 == false:
		entered1 = true
		global.bluescore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 2.tscn"))
		
	elif body.get_name() == "Pink" and entered1 == false:
		entered1 = true
		global.pinkscore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 2.tscn"))

func _on_Level2_body_entered(body):
	if body.get_name() == "Blue" and entered2 == false:
		entered2 = true
		global.bluescore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 3.tscn"))
	elif body.get_name() == "Pink":
		entered2 = true
		global.pinkscore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 3.tscn"))
		
func _on_Level3_body_entered(body):
	if body.get_name() == "PlayerBlueNew" and entered3 == false:
		entered3 = true
		global.bluescore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 4.tscn"))
	elif body.get_name() == "PlayerPinkNew" and entered3 == false:
		entered3 = true
		global.pinkscore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		print("yes")
		get_tree().change_scene(str("res://Scenes/Level 4.tscn"))
		
func _on_Level4_body_entered(body):
	if body.get_name() == "Blue" and entered4 == false:
		entered4 = true
		global.bluescore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 5.tscn"))
	elif body.get_name() == "Pink" and entered4 == false:
		entered4 = true
		global.pinkscore += 1
		$sfx_complete.play()
		yield($sfx_complete, "finished")
		get_tree().change_scene(str("res://Scenes/Level 5.tscn"))
